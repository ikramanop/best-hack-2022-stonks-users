package cmd

import (
	"log"
	"os"
	"strconv"

	"github.com/go-openapi/loads"
	"github.com/spf13/cobra"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/default_operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	auth2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/handlers/auth"
	_default "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/handlers/default"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/handlers/users"
)

var runServerCMD = &cobra.Command{
	Use:   "server",
	Short: "Run server",
	Long:  "Run server",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runServer()
	},
}

func runServer() error {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		return err
	}

	api := operations.NewAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			log.Fatalln(err)
		}
	}()

	port, err := strconv.Atoi(os.Getenv("STONKS_USERS_PORT"))
	if err != nil {
		return err
	}

	server.Port = port

	api.DefaultOperationsHealthCheckHandler = default_operations.HealthCheckHandlerFunc(_default.HealthCheckHandler)

	api.AuthCheckSessionHandler = auth.CheckSessionHandlerFunc(auth2.CheckSessionHandler)
	api.AuthLoginUserHandler = auth.LoginUserHandlerFunc(auth2.LoginUserHandler)

	api.UsersRegisterUserHandler = users.RegisterUserHandlerFunc(users2.RegisterUserHandler)
	api.UsersGetUserHandler = users.GetUserHandlerFunc(users2.GetUserHandler)
	api.UsersUpdateUserInfoHandler = users.UpdateUserInfoHandlerFunc(users2.UpdateUserInfoHandler)
	api.UsersGetPaymentInfoHandler = users.GetPaymentInfoHandlerFunc(users2.GetPaymentInfoHandler)
	api.UsersAddPaymentInfoHandler = users.AddPaymentInfoHandlerFunc(users2.AddPaymentInfoHandler)
	api.UsersDeletePaymentInfoHandler = users.DeletePaymentInfoHandlerFunc(users2.DeletePaymentInfoHandler)
	api.UsersChangeBalanceHandler = users.ChangeBalanceHandlerFunc(users2.ChangeBalanceHandler)
	api.UsersSwitchUserStateHandler = users.SwitchUserStateHandlerFunc(users2.SwitchUserStateHandler)
	api.UsersGetUsersWithParamsHandler = users.GetUsersWithParamsHandlerFunc(users2.GetUsersWithParamsHandler)
	api.UsersChangeUserRoleHandler = users.ChangeUserRoleHandlerFunc(users2.ChangeUserRoleHandler)

	if err := server.Serve(); err != nil {
		return err
	}

	return nil
}
