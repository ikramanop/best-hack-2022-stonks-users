begin;

create type status_type as enum ('active', 'blocked', 'deleted');

create type role_type as enum ('simple', 'verified', 'admin');

create table if not exists users
(
    id         serial      not null primary key,
    uuid       uuid        not null unique,
    email      text        not null unique,
    login      text        not null unique,
    password   text        not null,
    name       text        not null,
    surname    text        not null,
    patronymic text        null,
    balance    float       not null default 0,
    status     status_type not null default 'active',
    role       role_type   not null default 'simple',
    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);

create table if not exists payment_methods
(
    id          serial      not null primary key,
    user_id     integer     not null references users (id),
    card_number text        not null unique,
    month_till  smallint    not null,
    year_till   smallint    not null,
    card_holder text        not null,
    status      status_type not null default 'active',
    created_at  timestamptz not null default now(),
    updated_at  timestamptz not null default now()
);

commit;