begin;

drop table if exists payment_methods;
drop table if exists users;

drop type role_type;
drop type status_type;

commit;