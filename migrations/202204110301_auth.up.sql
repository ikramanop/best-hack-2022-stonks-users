begin;

create table if not exists auth_info
(
    id         serial primary key not null,
    user_id    integer            not null references users (id),
    token      text               not null,
    status     status_type        not null default 'active',
    created_at timestamptz        not null default now(),
    expire_at  timestamptz        not null
);

commit;