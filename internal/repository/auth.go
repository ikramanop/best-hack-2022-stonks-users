package repository

type Auth interface {
	Login(email string, password string) (string, bool, error)
	CheckSession(token string) (string, bool, error)
}
