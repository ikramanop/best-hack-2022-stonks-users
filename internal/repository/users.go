package repository

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

type Users interface {
	Create(user model.User) (model.User, bool, error)
	Get(userUUID string) (model.User, bool, error)
	Update(userUUID string, user model.User) (model.User, bool, error)
	UpdateRole(userUUID string, newRole string) (bool, error)
}

type UsersTx interface {
	ChangeBalance(userUUID string, balanceChange float64) (bool, error)
}
