package auth

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type Auth struct {
	*pgx.Conn
}

func New() (Auth, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_USERS_DSN"))
	if err != nil {
		return Auth{}, err
	}

	return Auth{db}, nil
}
