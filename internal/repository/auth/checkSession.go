package auth

import "context"

func (r *Auth) CheckSession(token string) (string, string, bool, error) {
	query := `
select u.uuid, u.role from users u
inner join auth_info ai on u.id = ai.user_id
where ai.token = $1
and ai.expire_at > now()
`

	var uuid string
	var role string
	err := r.QueryRow(context.Background(), query, token).Scan(&uuid, &role)
	if err != nil {
		return "", "", false, err
	}

	return uuid, role, true, nil
}
