package auth

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
	"time"
)

const tokenLength = 15

func (r *Auth) Login(login string, password string) (string, bool, error) {
	queryUser := `
SELECT id, password FROM users
WHERE login = $1
`

	var id uint64
	var pass string
	err := r.QueryRow(context.Background(), queryUser, login).Scan(&id, &pass)
	if err != nil {
		return "", false, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(pass), []byte(password))
	if err != nil {
		return "", false, err
	}

	queryDelete := `
DELETE FROM auth_info
WHERE user_id = $1
`

	_, err = r.Exec(context.Background(), queryDelete, id)
	if err != nil {
		return "", false, err
	}

	queryCreate := `
INSERT INTO auth_info (user_id, token, created_at, expire_at)
VALUES ($1, $2, $3, $4)
`

	token, err := generateSecureToken(tokenLength)
	if err != nil {
		return "", false, err
	}

	currentTime := time.Now()
	expireTime := currentTime.Add(time.Minute * 30)

	_, err = r.Exec(context.Background(), queryCreate, id, token, currentTime, expireTime)
	if err != nil {
		return "", false, err
	}

	return token, true, nil
}

func generateSecureToken(length int) (string, error) {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}
