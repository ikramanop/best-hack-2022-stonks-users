package payment_methods

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type PaymentMethods struct {
	*pgx.Conn
}

func New() (PaymentMethods, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_USERS_DSN"))
	if err != nil {
		return PaymentMethods{}, err
	}

	return PaymentMethods{db}, nil
}
