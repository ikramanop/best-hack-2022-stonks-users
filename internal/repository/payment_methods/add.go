package payment_methods

import (
	"context"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *PaymentMethods) Add(userUUID string, info model.PaymentInfo) (bool, error) {
	queryId := `
select id from users
where uuid = $1
`

	var id uint64
	err := r.QueryRow(context.Background(), queryId, userUUID).Scan(&id)
	if err != nil {
		return false, err
	}

	query := `
insert into payment_methods (user_id, card_number, month_till, year_till, card_holder)
values ($1, $2, $3, $4, $5)
`

	_, err = r.Exec(context.Background(), query, id, info.CardNumber, info.MonthTill,
		info.YearTill, info.CardHolder)
	if err != nil {
		return false, err
	}

	return true, nil
}
