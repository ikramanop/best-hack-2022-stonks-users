package payment_methods

import (
	"context"
)

func (r *PaymentMethods) Delete(userUUID string, cardNumber string) (bool, error) {
	queryId := `
select id from users
where uuid = $1
`

	var id uint64
	err := r.QueryRow(context.Background(), queryId, userUUID).Scan(&id)
	if err != nil {
		return false, err
	}

	query := `
UPDATE payment_methods
SET status = 'deleted',
updated_at = now()
WHERE user_id = $1 AND
card_number = $2
`

	_, err = r.Exec(context.Background(), query, id, cardNumber)
	if err != nil {
		return false, err
	}

	return true, nil
}
