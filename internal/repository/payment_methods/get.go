package payment_methods

import (
	"context"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *PaymentMethods) Get(userUUID string) ([]model.PaymentInfo, bool, error) {
	queryPaymentMethods := `
SELECT pm.id, pm.user_id,
pm.card_number, pm.month_till, pm.year_till, pm.card_holder,
pm.status, pm.created_at, pm.updated_at
FROM payment_methods pm
INNER JOIN users u ON pm.user_id = u.id
WHERE u.uuid = $1 AND pm.status != 'deleted'
`

	var paymentMethods []model.PaymentInfo
	rows, err := r.Query(context.Background(), queryPaymentMethods, userUUID)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var paymentInfo model.PaymentInfo
		err := rows.
			Scan(&paymentInfo.ID, &paymentInfo.UserID,
				&paymentInfo.CardNumber, &paymentInfo.MonthTill, &paymentInfo.YearTill, &paymentInfo.CardHolder,
				&paymentInfo.Status, &paymentInfo.CreatedAt, &paymentInfo.UpdatedAt)
		if err != nil {
			return nil, false, err
		}
		paymentMethods = append(paymentMethods, paymentInfo)
	}

	return paymentMethods, true, nil
}
