package repository

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

type PaymentMethods interface {
	Add(userUUID string, info model.PaymentInfo) (bool, error)
	Get(userUUID string) (model.PaymentInfo, bool, error)
	Delete(userUUID string, cardNumber string) (bool, error)
}
