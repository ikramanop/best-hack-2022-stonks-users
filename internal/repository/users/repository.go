package users

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type Users struct {
	*pgx.Conn
}

type UsersTx struct {
	pgx.Tx
}

func New() (Users, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_USERS_DSN"))
	if err != nil {
		return Users{}, err
	}

	return Users{db}, nil
}

func NewTx(tx pgx.Tx) UsersTx {
	return UsersTx{tx}
}
