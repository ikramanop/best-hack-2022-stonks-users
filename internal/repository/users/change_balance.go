package users

import (
	"context"
)

func (r *UsersTx) ChangeBalance(userUUID string, balanceChanged float64) (bool, error) {
	query := `
update users
set balance = $1
where uuid = $2
`

	_, err := r.Exec(context.Background(), query, balanceChanged, userUUID)
	if err != nil {
		return false, err
	}

	return true, nil
}
