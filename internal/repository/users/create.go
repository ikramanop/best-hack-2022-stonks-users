package users

import (
	"context"

	"github.com/gofrs/uuid"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *Users) Create(user model.User) (model.User, bool, error) {
	queryUsers := `
insert into users (uuid, login, email, password,
name, surname, patronymic)
values ($1, $2, $3, $4, $5, $6, $7)
`

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return model.User{}, false, err
	}

	uuidGen, err := uuid.NewV4()
	if err != nil {
		return model.User{}, false, err
	}

	_, err = r.Exec(context.Background(), queryUsers, uuidGen.String(), user.Login, user.Email, hash,
		user.Name, user.Surname, user.Patronymic)
	if err != nil {
		return model.User{}, false, err
	}

	return r.Get(uuidGen.String())
}
