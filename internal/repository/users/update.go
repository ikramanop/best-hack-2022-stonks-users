package users

import (
	"context"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *Users) Update(userUUID string, name string, surname string, patronymic *string) (model.User, bool, error) {
	query := `
UPDATE users
SET name = $1, surname = $2, patronymic = $3, updated_at = now()
WHERE uuid = $4
`

	_, err := r.Exec(context.Background(), query, name, surname, patronymic, userUUID)
	if err != nil {
		return model.User{}, false, err
	}

	return r.Get(userUUID)
}
