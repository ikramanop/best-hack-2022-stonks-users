package users

import (
	"context"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *Users) SwitchState(userUUID string) (bool, error) {
	user, ok, err := r.Get(userUUID)
	if err != nil {
		return false, err
	}
	if !ok {
		return ok, nil
	}

	var targetState model.UserStatus
	switch user.Status {
	case model.UserStatusActive:
		targetState = model.UserStatusBlocked
	case model.UserStatusBlocked:
		targetState = model.UserStatusActive
	default:
		return true, nil
	}

	query := `
update users set status = $1
where user_uuid = $2
`

	_, err = r.Exec(context.Background(), query, targetState, userUUID)
	if err != nil {
		return false, err
	}

	return true, nil
}
