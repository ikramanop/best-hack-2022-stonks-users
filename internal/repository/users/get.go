package users

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *Users) Get(userUUID string) (model.User, bool, error) {
	queryUsers := `
SELECT id, uuid, login, email, password,
name, surname, patronymic, balance,
role, status, role, created_at, updated_at
FROM users
WHERE uuid = $1 AND status != 'deleted'
`
	var user model.User
	err := r.QueryRow(context.Background(), queryUsers, userUUID).
		Scan(&user.ID, &user.UUID, &user.Login, &user.Email, &user.Password,
			&user.Name, &user.Surname, &user.Patronymic, &user.Balance,
			&user.Role, &user.Status, &user.Role, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.User{}, false, nil
		}

		return model.User{}, false, err
	}

	queryPaymentMethods := `
SELECT pm.id, pm.user_id,
pm.card_number, pm.month_till, pm.year_till, pm.card_holder,
pm.status, pm.created_at, pm.updated_at
FROM payment_methods pm
INNER JOIN users u ON pm.user_id = u.id
WHERE u.uuid = $1 AND pm.status != 'deleted'
`

	var paymentMethods []model.PaymentInfo
	rows, err := r.Query(context.Background(), queryPaymentMethods, userUUID)
	if err != nil {
		return model.User{}, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var paymentInfo model.PaymentInfo
		err := rows.
			Scan(&paymentInfo.ID, &paymentInfo.UserID,
				&paymentInfo.CardNumber, &paymentInfo.MonthTill, &paymentInfo.YearTill, &paymentInfo.CardHolder,
				&paymentInfo.Status, &paymentInfo.CreatedAt, &paymentInfo.UpdatedAt)
		if err != nil {
			return model.User{}, false, err
		}
		paymentMethods = append(paymentMethods, paymentInfo)
	}

	user.PaymentMethods = paymentMethods

	return user, true, nil
}
