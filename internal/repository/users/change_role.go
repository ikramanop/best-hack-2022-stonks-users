package users

import "context"

func (r *Users) ChangeRole(userUUID string, newRole string) (bool, error) {
	query := `
update users set role = $1
where user_uuid = $2
`

	_, err := r.Exec(context.Background(), query, newRole, userUUID)
	if err != nil {
		return false, err
	}

	return true, nil
}
