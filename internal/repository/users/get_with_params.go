package users

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *Users) GetWithParams(roles []string, status string) ([]model.User, bool, error) {
	queryUsers := `
SELECT id, uuid, login, email, password,
name, surname, patronymic, balance,
role, status, role, created_at, updated_at
FROM users
WHERE status = $1
and role in any ($2)
`
	rows, err := r.Query(context.Background(), queryUsers, status, roles)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, false, nil
		}

		return nil, false, err
	}
	defer rows.Close()

	var users []model.User
	for rows.Next() {
		var user model.User
		err := rows.Scan(&user.ID, &user.UUID, &user.Login, &user.Email, &user.Password,
			&user.Name, &user.Surname, &user.Patronymic, &user.Balance,
			&user.Role, &user.Status, &user.Role, &user.CreatedAt, &user.UpdatedAt)
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, false, nil
			}

			return nil, false, err
		}

		users = append(users, user)
	}

	return users, true, nil
}
