package users

import (
	"context"
	"errors"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
)

func (r *Users) UpdateRole(userUUID string, newRole string) (bool, error) {
	switch model.UserRole(newRole) {
	case model.UserRoleSimple, model.UserRoleAdmin, model.UserRoleVerified:
	default:
		return false, errors.New("unsupported role")
	}

	query := `
UPDATE users
SET role = $1, updated_at = now()
WHERE uuid = $2
`

	_, err := r.Exec(context.Background(), query, newRole, userUUID)
	if err != nil {
		return false, err
	}

	return true, nil
}
