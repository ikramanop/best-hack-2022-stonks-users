package model

import "time"

type User struct {
	ID uint64 `db:"id"`

	UUID string `db:"uuid"`

	Login string `db:"login"`

	Email string `db:"email"`

	Password string `db:"password"`

	Name string `db:"name"`

	Surname string `db:"surname"`

	Patronymic *string `db:"patronymic"`

	Balance float64 `db:"balance"`

	PaymentMethods []PaymentInfo

	Status UserStatus `db:"status"`

	Role UserRole `db:"role"`

	CreatedAt time.Time `db:"created_at"`

	UpdatedAt time.Time `db:"updated_at"`
}

type UserStatus string

const (
	UserStatusActive  UserStatus = "active"
	UserStatusDeleted UserStatus = "deleted"
	UserStatusBlocked UserStatus = "blocked"
)

type UserRole string

const (
	UserRoleSimple   UserRole = "simple"
	UserRoleVerified UserRole = "verified"
	UserRoleAdmin    UserRole = "admin"
)
