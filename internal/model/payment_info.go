package model

import "time"

type PaymentInfo struct {
	ID uint64 `db:"id"`

	UserID uint64 `db:"user_id"`

	CardNumber string `db:"card_number"`

	MonthTill uint8 `db:"month_till"`

	YearTill uint16 `db:"year_till"`

	CardHolder string `db:"card_holder"`

	Status PaymentInfoStatus `db:"status"`

	CreatedAt time.Time `db:"created_at"`

	UpdatedAt time.Time `db:"updated_at"`
}

type PaymentInfoStatus string

const (
	PaymentInfoStatusActive  PaymentInfoStatus = "active"
	PaymentInfoStatusExpired PaymentInfoStatus = "expired"
	PaymentInfoStatusDeleted PaymentInfoStatus = "deleted"
)
