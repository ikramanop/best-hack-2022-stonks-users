package users

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
	"log"
)

func GetUsersWithParamsHandler(params users.GetUsersWithParamsParams) middleware.Responder {
	log.Printf("Hit POST /user/{uuid}/get-with-params from %s\n", params.HTTPRequest.UserAgent())

	rx, err := users2.New()
	if err != nil {
		return users.NewGetUsersWithParamsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	usersRaw, ok, err := rx.GetWithParams(params.Body.Roles, params.Body.Status)
	if err != nil {
		return users.NewGetUsersWithParamsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewGetUsersWithParamsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "users not found",
				Status:  false,
			},
		)
	}

	var usersResponses []*models.UserWithUUID
	for _, userRaw := range usersRaw {
		usersResponses = append(usersResponses, &models.UserWithUUID{
			Balance:    userRaw.Balance,
			Email:      userRaw.Email,
			Login:      userRaw.Login,
			Name:       userRaw.Name,
			Patronymic: userRaw.Patronymic,
			Role:       string(userRaw.Role),
			Surname:    userRaw.Surname,
			UUID:       userRaw.UUID,
		})
	}

	return users.NewGetUsersWithParamsOK().WithPayload(&models.UsersWithUUID{Users: usersResponses})
}
