package users

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/payment_methods"
)

func GetPaymentInfoHandler(params users.GetPaymentInfoParams) middleware.Responder {
	log.Printf("Hit GET /user/{uuid}/paymentInfo from %s\n", params.HTTPRequest.UserAgent())

	rx, err := payment_methods.New()
	if err != nil {
		return users.NewGetPaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	paymentMethodsRaw, ok, err := rx.Get(params.UUID)
	if err != nil {
		return users.NewGetPaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewGetPaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "payment methods not found",
				Status:  false,
			},
		)
	}

	var paymentMethods []*models.PaymentInfo
	if paymentMethodsRaw != nil {
		paymentMethods = make([]*models.PaymentInfo, 0)

		for _, paymentInfo := range paymentMethodsRaw {
			paymentMethods = append(paymentMethods, &models.PaymentInfo{
				CardHolder: paymentInfo.CardHolder,
				CardNumber: paymentInfo.CardNumber,
				Status:     string(paymentInfo.Status),
				TillDate: &models.PaymentInfoTillDate{
					Month: int64(paymentInfo.MonthTill),
					Year:  int64(paymentInfo.YearTill),
				},
			})
		}
	}

	return users.NewGetPaymentInfoOK().WithPayload(
		&users.GetPaymentInfoOKBody{
			Payments: paymentMethods,
		},
	)
}
