package users

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
	"log"
)

func ChangeUserRoleHandler(params users.ChangeUserRoleParams) middleware.Responder {
	log.Printf("Hit PATCH /user/{uuid}/changeRole from %s\n", params.HTTPRequest.UserAgent())

	rx, err := users2.New()
	if err != nil {
		return users.NewChangeUserRoleBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	ok, err := rx.ChangeRole(params.UUID, params.NewRole)
	if err != nil {
		return users.NewChangeUserRoleBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewChangeUserRoleBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "cant change user role",
				Status:  false,
			},
		)
	}

	return users.NewChangeUserRoleOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
