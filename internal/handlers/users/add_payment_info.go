package users

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/payment_methods"
)

func AddPaymentInfoHandler(params users.AddPaymentInfoParams) middleware.Responder {
	log.Printf("Hit PUT /user/{uuid}/paymentInfo from %s\n", params.HTTPRequest.UserAgent())

	rx, err := payment_methods.New()
	if err != nil {
		return users.NewAddPaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	ok, err := rx.Add(params.UUID, model.PaymentInfo{
		CardNumber: params.Body.CardNumber,
		MonthTill:  uint8(params.Body.TillDate.Month),
		YearTill:   uint16(params.Body.TillDate.Year),
		CardHolder: params.Body.CardHolder,
	})
	if err != nil {
		return users.NewAddPaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewAddPaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "payment methods not found",
				Status:  false,
			},
		)
	}

	return users.NewAddPaymentInfoOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
