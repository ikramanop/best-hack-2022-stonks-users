package users

import (
	"errors"
	"log"
	"net/mail"
	"unicode"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/model"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
)

func RegisterUserHandler(params users.RegisterUserParams) middleware.Responder {
	log.Printf("Hit POST /user/register from %s\n", params.HTTPRequest.UserAgent())

	if err := verifyParams(params); err != nil {
		return users.NewRegisterUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	rx, err := users2.New()
	if err != nil {
		return users.NewRegisterUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	_, ok, err := rx.Create(model.User{
		Login:      params.Body.Login,
		Email:      params.Body.Email,
		Password:   params.Body.Password,
		Name:       params.Body.Name,
		Surname:    params.Body.Surname,
		Patronymic: params.Body.Patronymic,
	})
	if err != nil {
		return users.NewRegisterUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewRegisterUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "cant create user",
				Status:  false,
			},
		)
	}

	return users.NewRegisterUserOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}

func verifyParams(params users.RegisterUserParams) error {
	_, err := mail.ParseAddress(params.Body.Email)
	if err != nil {
		return errors.New("incorrect email")
	}

	if !verifyPassword(params.Body.Password) {
		return errors.New("incorrect password")
	}

	if len(params.Body.Login) < 6 || len(params.Body.Login) > 16 {
		return errors.New("incorrect login")
	}

	return nil
}

func verifyPassword(s string) bool {
	if len(s) < 8 || len(s) > 128 {
		return false
	}

	var number, special, upper, valid bool
	valid = true

	for _, c := range s {
		if unicode.IsNumber(c) {
			number = true
			continue
		}
		if unicode.IsUpper(c) {
			upper = true
			continue
		}

		if isSpecial(c) {
			special = true
			continue
		}

		if c == ' ' {
			valid = false
			continue
		}
	}

	return number && special && upper && valid
}

func isSpecial(s rune) bool {
	symbols := []rune{'~', '!', '@', '#', '$', '%', '^', '&', '*',
		'_', '-', '+', '=', '\'', '|', '\'', '{', '}', ']',
		':', '"', '.', '<', '>', '.', '?', '/'}
	for _, sym := range symbols {
		if s == sym {
			return true
		}
	}

	return false
}
