package users

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/payment_methods"
)

func DeletePaymentInfoHandler(params users.DeletePaymentInfoParams) middleware.Responder {
	log.Printf("Hit DELETE /user/{uuid}/paymentInfo from %s\n", params.HTTPRequest.UserAgent())

	rx, err := payment_methods.New()
	if err != nil {
		return users.NewDeletePaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	ok, err := rx.Delete(params.UUID, params.Body.CardNumber)
	if err != nil {
		return users.NewDeletePaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewDeletePaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "payment methods not found",
				Status:  false,
			},
		)
	}

	return users.NewDeletePaymentInfoOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
