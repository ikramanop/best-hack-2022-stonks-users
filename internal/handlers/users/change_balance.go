package users

import (
	"context"
	"github.com/jackc/pgx/v4"
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
)

func ChangeBalanceHandler(params users.ChangeBalanceParams) middleware.Responder {
	log.Printf("Hit PATCH /user/{uuid}/balance from %s\n", params.HTTPRequest.UserAgent())

	rx, err := users2.New()
	if err != nil {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	user, ok, err := rx.Get(params.UUID)
	if err != nil {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "user not found",
				Status:  false,
			},
		)
	}

	if (user.Balance + params.Body.BalanceChange) < 0 {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "cant go lower in balance",
				Status:  false,
			},
		)
	}

	tx, err := rx.BeginTx(context.Background(), pgx.TxOptions{})
	if err != nil {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	defer func() {
		if err != nil {
			tx.Rollback(context.TODO())
		} else {
			tx.Commit(context.TODO())
		}
	}()

	txt := users2.NewTx(tx)

	ok, err = txt.ChangeBalance(params.UUID, user.Balance+params.Body.BalanceChange)
	if err != nil {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "cant update balance",
				Status:  false,
			},
		)
	}

	return users.NewChangeBalanceOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
