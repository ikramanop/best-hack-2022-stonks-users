package users

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
)

func UpdateUserInfoHandler(params users.UpdateUserInfoParams) middleware.Responder {
	log.Printf("Hit PATCH /user/{uuid} from %s\n", params.HTTPRequest.UserAgent())

	rx, err := users2.New()
	if err != nil {
		return users.NewUpdateUserInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	_, ok, err := rx.Update(params.UUID, params.Body.Name, params.Body.Surname, params.Body.Patronymic)
	if err != nil {
		return users.NewUpdateUserInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewUpdateUserInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "user not found",
				Status:  false,
			},
		)
	}

	return users.NewUpdateUserInfoOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
