package users

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
)

func GetUserHandler(params users.GetUserParams) middleware.Responder {
	log.Printf("Hit GET /user/{uuid} from %s\n", params.HTTPRequest.UserAgent())

	rx, err := users2.New()
	if err != nil {
		return users.NewGetUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	user, ok, err := rx.Get(params.UUID)
	if err != nil {
		return users.NewGetUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewGetUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "user not found",
				Status:  false,
			},
		)
	}

	var paymentMethods []*models.PaymentInfo
	if user.PaymentMethods != nil {
		paymentMethods = make([]*models.PaymentInfo, 0)

		for _, paymentInfo := range user.PaymentMethods {
			paymentMethods = append(paymentMethods, &models.PaymentInfo{
				CardHolder: paymentInfo.CardHolder,
				CardNumber: paymentInfo.CardNumber,
				Status:     string(paymentInfo.Status),
				TillDate: &models.PaymentInfoTillDate{
					Month: int64(paymentInfo.MonthTill),
					Year:  int64(paymentInfo.YearTill),
				},
			})
		}
	}

	return users.NewGetUserOK().WithPayload(
		&models.User{
			Balance:        user.Balance,
			Login:          user.Login,
			Email:          user.Email,
			Role:           string(user.Role),
			Name:           user.Name,
			Patronymic:     user.Patronymic,
			PaymentMethods: paymentMethods,
			Surname:        user.Surname,
		},
	)
}
