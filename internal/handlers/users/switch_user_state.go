package users

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/users"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/users"
	"log"
)

func SwitchUserStateHandler(params users.SwitchUserStateParams) middleware.Responder {
	log.Printf("Hit PATCH /user/{uuid}/switch from %s\n", params.HTTPRequest.UserAgent())

	rx, err := users2.New()
	if err != nil {
		return users.NewSwitchUserStateBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	ok, err := rx.SwitchState(params.UUID)
	if err != nil {
		return users.NewSwitchUserStateBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return users.NewSwitchUserStateBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unable to switch state",
				Status:  false,
			},
		)
	}

	return users.NewSwitchUserStateOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
