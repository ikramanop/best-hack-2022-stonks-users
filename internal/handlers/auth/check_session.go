package auth

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/auth"
	auth2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/auth"
)

func CheckSessionHandler(params auth.CheckSessionParams) middleware.Responder {
	log.Printf("Hit POST /auth/checkSession from %s\n", params.HTTPRequest.UserAgent())

	rx, err := auth2.New()
	if err != nil {
		return auth.NewCheckSessionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	uuid, role, ok, err := rx.CheckSession(params.Request.Token)
	if err != nil {
		return auth.NewCheckSessionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return auth.NewCheckSessionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "session not found",
				Status:  false,
			},
		)
	}

	return auth.NewCheckSessionOK().WithPayload(
		&models.TokenResponse{
			Status:   true,
			UserUUID: &uuid,
			Role:     &role,
		},
	)
}
