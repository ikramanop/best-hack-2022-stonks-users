package auth

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/generated/restapi/operations/auth"
	auth2 "gitlab.com/ikramanop/best-hack-2022-stonks-users/internal/repository/auth"
)

func LoginUserHandler(params auth.LoginUserParams) middleware.Responder {
	log.Printf("Hit POST /auth/login from %s\n", params.HTTPRequest.UserAgent())

	rx, err := auth2.New()
	if err != nil {
		return auth.NewLoginUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	uuid, ok, err := rx.Login(params.Body.Login, params.Body.Password)
	if err != nil {
		return auth.NewLoginUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return auth.NewLoginUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "login failed",
				Status:  false,
			},
		)
	}

	return auth.NewLoginUserOK().WithPayload(
		&models.TokenResponse{
			Status:   true,
			UserUUID: &uuid,
		},
	)
}
