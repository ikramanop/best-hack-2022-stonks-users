# BEST HACK 2022 Stonks Users

## Сервис для работы с пользовательскими данными

Stonks Users хранит и изменяет информацию о пользователях, авторизирует и аутентифицирует их.

## Функционал

- Регистрация и авторизация пользователей
- Хранение и изменение денежного баланса пользователя
- Хранение сессии пользователя
- Изменение ролей пользователя
- Изменения состояния пользователя

## Cтек

- Golang
- Swagger OpenAPI
- PostgreSQL

## Как поднять?

Создайте docker-сеть

```bash
docker network create stonks-network
```

Поднимите приложение со всеми зависимостями

```bash
make run-app
```

Завершите работу приложения

```bash
make stop-app
```

Является частью полноценной биржевой системы Stonks

## Как пользоваться?

В рамках "большого" API [Stonks API] https://gitlab.com/ikramanop/best-hack-2022-stonks-api